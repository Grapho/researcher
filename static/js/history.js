function clear_confirm() {
    if (confirm("Уверены, что хотите очистить историю?")) {
        fetch("/clear_history", {method: "POST"}).finally(() => {location.reload()});
    }
}