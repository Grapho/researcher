let sites_list = document.querySelector(".sites #list")
let words_list = document.querySelector(".words #list")

let sites_radio_file = document.querySelector("input#file_mode")
let sites_radio_list = document.querySelector("input#list_mode")

sites_radio_file.addEventListener("click", change_input_sites);
sites_radio_list.addEventListener("click", change_input_sites);

function change_input_sites(elem) {
    if (elem.srcElement.value == "file") {
        document.querySelector(".sites #list").classList.add("hidden")
        document.querySelector(".sites #file").classList.remove("hidden")
    } else {
        document.querySelector(".sites #list").classList.remove("hidden")
        document.querySelector(".sites #file").classList.add("hidden")
    }
}