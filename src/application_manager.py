import os
from flask import Flask

from flask_sqlalchemy import SQLAlchemy


class Application:
    upload_folder = f'static/uploads/'
    if not os.path.exists(upload_folder):
        os.makedirs(upload_folder)

    app = Flask(
        __name__,
        template_folder='../templates',
        static_folder="../static")
    app.config['SQLALCHEMY_TRACK_MODIFICATIONS'] = True
    app.config['API_KEY'] = 'f93412b5-b380-4420-b65f-adc694195102'
    app.config['SQLALCHEMY_DATABASE_URI'] = "sqlite:///../database.sqlite"

    db = SQLAlchemy(app)
