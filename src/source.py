import os
import time
import pandas as pd

from datetime import datetime

from src.models import Search, SearchResult
from src.application_manager import Application

db = Application.db


def prepare_words(words):
    words = words.replace("\n", "")
    words = words.replace("\t", "")
    words = words.replace("\r", "")
    words = words.split(",")
    words += [w.upper() for w in words]
    words += [w.lower() for w in words]
    words += [w.replace(letter, letter.upper()) for w in words for letter in w]
    words += [w.replace(letter, letter.lower()) for w in words for letter in w]
    return set(words)


def do_clear_history():
    Search.query.delete()
    SearchResult.query.delete()
    db.session.commit()


def get_report(id):
    target = Search.query.get(id)
    filename = f"report_{target.date.strftime('%d-%m-%Y')}.xlsx"
    data = list()
    for item in target.results:
        data.append([
            item.word,
            item.title,
            item.header,
            item.text,
            item.link
        ])
    df = pd.DataFrame(
        data=data,
        columns=[
            "Слово",
            "Подзаголовок",
            "Заголовок",
            "Выдержка",
            "Ссылка на источник"
        ]
    )
    df.to_excel(Application.upload_folder + filename)
    while True:
        time.sleep(1)
        if os.path.exists(Application.upload_folder + filename):
            return filename


def do_search_from_file(file, words):
    file.save(os.path.join(
        Application.upload_folder,
        file.filename))
    file_data = pd.read_excel(file)
    search = Search(
        source=file.filename,
        date=datetime.utcnow())
    for word in prepare_words(words):
        text = file_data['Выдержки из текста'].str
        if text.contains(word).any():
            for _, row in file_data[text.contains(word)].iterrows():
                result = SearchResult(
                    word=word,
                    header=row["Заголовок"],
                    title=row["Подзаголовок"],
                    text=row['Выдержки из текста'],
                    link=row["Ссылка на источник"],
                    search=search
                )
                search.result = "Есть совпадения"
                db.session.add(result)
    db.session.add(search)
    db.session.commit()


def do_search_from_sites(sites, words):
    print(sites, words)
