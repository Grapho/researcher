from datetime import datetime

from .application_manager import Application

db = Application.db


class Search(db.Model):
    __tablename__ = "search"
    id = db.Column(db.Integer, primary_key=True)
    date = db.Column(db.DateTime, default=datetime.utcnow())
    source = db.Column(db.String)
    result = db.Column(db.String, default="Совпадений не найдено")


class SearchResult(db.Model):
    __tablename__ = "search_result"
    id = db.Column(db.Integer, primary_key=True)
    word = db.Column(db.String)
    header = db.Column(db.String)
    title = db.Column(db.String)
    text = db.Column(db.String)
    link = db.Column(db.String)

    search = db.relationship(Search, backref="results")
    search_id = db.Column(db.Integer, db.ForeignKey("search.id"))
