FROM python:3.6
WORKDIR /app
COPY requirements.txt .
RUN python -m pip install -r requirements.txt
COPY . .
ENTRYPOINT ["python"]
CMD ["./server_app.py"]