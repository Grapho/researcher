from flask import (
    request, render_template,
    redirect, url_for, send_file
)

from src.source import (
    do_search_from_file, do_search_from_sites,
    do_clear_history, get_report
)

from src.models import Search, SearchResult
from src.application_manager import Application

app = Application.app


@app.route('/')
def main_view():
    return render_template('index.html')


@app.route('/history')
def history_view():
    return render_template(
        'history.html',
        items=Search.query.all())


@app.route('/item_history/<id>')
def item_history_view(id):
    return render_template(
        'item_history.html',
        items=Search.query.get(id).results,
        id=id)


@app.route('/more/<id>')
def show_more(id):
    return render_template(
        'more.html',
        item=SearchResult.query.get(id))


@app.route('/configuration')
def configuration_view():
    return render_template('configuration.html')


@app.route('/error')
def error_view():
    return render_template('error.html')


# ================== [POSTS] ==================


@app.route('/do_search', methods=["POST"])
def do_search():
    mode = request.form.get("mode")
    sites = request.form.get("sites")
    words = request.form.get("words")
    file = request.files.get("file")

    if not words:
        return redirect(url_for("error_view"))

    if mode == "list":
        if not sites:
            return redirect(url_for("error_view"))
        do_search_from_sites(sites=sites, words=words)
    if mode == "file":
        if not file:
            return redirect(url_for("error_view"))
        do_search_from_file(file=file, words=words)

    return redirect(url_for("history_view"))


@app.route('/clear_history', methods=["POST"])
def clear_history():
    do_clear_history()
    return redirect(url_for("history_view"))


@app.route('/export_report/<id>', methods=["POST"])
def export_report(id):
    filename = get_report(id)
    return send_file(
        f"../{Application.upload_folder + filename}",
        as_attachment=True,
        attachment_filename=filename)


if __name__ == "__main__":
    Application.db.create_all()
    app.run(host='0.0.0.0', port=8080, debug=False)
